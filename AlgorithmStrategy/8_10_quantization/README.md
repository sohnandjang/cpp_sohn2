# ret = quantize(int idx, int group)
1. idx: current index of input  
2. group : remained group of whole  
3. ret : minimum quantization error  

example) 5 4 3 2 1  
1) sort : 1 2 3 4 5  
2) group : 2  
3) quantize(0, 2)  

```
Q(0, 2)
  = squareError(0, 0) + Q(1, 1); // base condition)
                                Q(1,1) = squareError(1, N-1) 
                                if group == 1 

  = squareError(0, 1) + Q(2, 1);
  
  = squareError(0, 2) + Q(3, 1);

  = squareError(0, 3) + Q(4, 1);

  = squareError(0, 4) + Q(5, 1); --> NG : base condition) 
                                          idx >= N-group+1 : NG
```
