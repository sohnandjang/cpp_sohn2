#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <stdio.h>
#include <limits.h>
using namespace std;

int N;
int G;
vector<int> input;
int maximum = 987654321;
int cache[102][12];

int squareError(int start, int end)
{
	int average1 = 0;
	int average2 = 0;
	int sum = 0;
	int squareSum1 = 0;
	int squareSum2 = 0;
	int squareSum = 0;
	int len = end - start + 1;
	for (int i = start; i <= end; i++)
		sum += input[i];
	average1 = sum / len;
	average2 = sum / len + 1;
	for (int i = start; i <= end; i++)
		squareSum1 += (input[i] - average1)*(input[i] - average1);
	for (int i = start; i <= end; i++)
		squareSum2 += (input[i] - average2)*(input[i] - average2);

	squareSum = min(squareSum1, squareSum2);
	return squareSum;
}
int quantize(int idx, int group)
{
	//printf("idx:%d, group:%d\n", idx, group);
	int& ret = cache[idx][group];
	if (ret != -1)
	{
		//printf("use cache\n");
		return ret;
	}
	ret = maximum;

	if (idx >= (N - group + 1)) return ret = maximum;
	if (group == 1)
	{
		return ret = squareError(idx, N - 1);
	}


	//~ body
	for (int i = 0; i < N - idx; i++)
	{
		ret = min(ret, squareError(idx, idx+i) + quantize(idx+i+1, group - 1));
	}

	return ret;
}

void disp()
{
	cout << "input:";
	for (int i = 0; i < input.size(); i++)
		cout << input[i] << ", ";
	cout << endl;
}
void body(fstream& fs)
{
	// init
	input.clear();
	memset(cache, -1, sizeof(cache));
	// input
	fs >> N;
	fs >> G;
	for (int i = 0; i < N; i++){
		int temp;
		fs >> temp;
		input.push_back(temp);
	}
	//disp();
	sort(input.begin(), input.end());
	//disp();
	// body function
	int ret;
	cout << "quantize:" <<"N="<<N<<", G=" << G<< endl;
	ret = quantize(0, G);
	cout << ret << endl;
}

bool caseSolve(const char* path, int i_num) 
{// i_num == -1 , don't care
	fstream myfile;
	int num = 0;
	string abc;
	myfile.open(path);
	if (myfile.is_open())
	{
		myfile >> num;
		num = (i_num == -1 ? num : i_num);
		for (int i = 0; i < num; i++)
		{ //~ body
			body(myfile);
		}
		myfile.close();
		return true;
	}
	return false;
}

int main()
{
	caseSolve("quantization.txt", -1);
	getchar();
	return 0;
}
