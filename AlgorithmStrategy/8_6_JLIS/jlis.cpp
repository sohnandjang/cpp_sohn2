#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

vector<int> vec_jlis;
vector<int> vec_A;
vector<int> vec_B;
int length_A = 0;
int length_B = 0;
int cache[101][101];
long long NEGINF = numeric_limits<long long>::min();
int jlis(int indexA, int indexB)
{
	int &ret = cache[indexA+1][indexB+1]; 
	if (ret != -1)
		return ret;
	ret = 2;
	printf(">>> indexA = %d, indexB = %d \n", indexA, indexB);
	//printf(">>>	[%lld, %lld]\n", indexA==-1?NEGINF:vec_A[indexA], indexB==-1?NEGINF:vec_B[indexB]);
	long long maxA = ((indexA == -1) ? NEGINF : vec_A[indexA]);
	long long maxB = ((indexB == -1) ? NEGINF : vec_B[indexB]);
	long long maxElement = max(maxA, maxB);
	//printf("maxElement=%ld, maxA=%ld, maxB=%ld\n", maxElement, maxA, maxB);
	for (int nextA = indexA + 1; nextA < length_A; nextA++)
	{
		//printf("nextA = %d\n", nextA);
		if (maxElement < vec_A[nextA])
		{
			//printf("*** maxElement = %ld\n", maxElement); 
			//printf("*** nextA = %d\n", nextA);
			//printf("*** vec_A[nextA] = %ld\n", vec_A[nextA]);
			ret = max(ret, 1 + jlis(nextA, indexB));
			//printf("ret = %ld\n", ret);
		}
	}
	for (int nextB = indexB + 1; nextB < length_B; nextB++)
	{
		if (maxElement < vec_B[nextB])
		{
			//printf("maxElement = %ld < vec_B[%d]=%ld\n", maxElement, nextB, vec_B[nextB]);
			ret = max(ret, 1 + jlis(indexA, nextB));
			//printf("ret = %ld\n", ret);
		}
	}
	ret = max(ret, ret);

	//printf("ret = %ld\n", ret);
	return 	ret;
}

void body(fstream& fs)
{
	// init
	length_A = 0;
	length_B = 0;
	vec_A.clear();
	vec_B.clear();
	vec_jlis.clear();
	memset(cache, -1, sizeof(cache));

	// input
	fs >> length_A;
	fs >> length_B;
	int temp = 0;
	
	for (int i = 0; i < length_A; i++)
	{
		fs >> temp;
		vec_A.push_back(temp);
	}
	for (int i = 0; i < length_B; i++)
	{
		fs >> temp;
		vec_B.push_back(temp);
	}
	// body function
	int ret = 0;
	ret = jlis(-1, -1);
	cout << ret - 2 << endl;
}

bool caseSolve(const char* path, int i_num) 
{// i_num == -1 , don't care
	fstream myfile;
	int num = 0;
	myfile.open(path);
	if (myfile.is_open())
	{
		myfile >> num;
		num = (i_num == -1 ? num : i_num);
		for (int i = 0; i < num; i++)
		{ //~ body
			body(myfile);
		}
		myfile.close();
		return true;
	}
	return false;
}

int main()
{
	caseSolve("jlis.txt", -1);
	getchar();
	return 0;
}