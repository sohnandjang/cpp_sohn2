#include <iostream>
#include <algorithm>
using namespace std;

int S[8] = { 3, 2, 1, 7, 5, 4, 2, 6 };
int n = 8;

int lis2(int start)
{
	printf("lis2 : start:%d, S[%d]=%d\n", start, start, start == -1 ? -1 : S[start]);
	int ret = 1;
	//cout << "for start: "<< endl;
	for (int next = start + 1; next < n; ++next)
	{
		printf("*** try: next = %d, S[next]=%d\n", next, start==-1? -1 : S[next]);
		if (S[start] < S[next])
		{
			ret = max(ret, lis2(next) + 1);
		}
	}

	//cout << "for end: "<< endl;
	return ret;
}
int lis3(int start)
{
	printf(">>>>> lis3 : start:%d, S[%d]=%d\n", start, start, start == -1 ? -1 : S[start]);
	int ret = 1;
	//cout << "for start: "<< endl;
	for (int next = start + 1; next < n; ++next)
	{
		printf("*** try: next = %d, S[next]=%d\n", next, start==-1? -1 : S[next]);
		if (start == -1 || S[start] < S[next])
		{
			ret = max(ret, lis3(next) + 1);
			//printf("====================================\n");
		}
	}

	//cout << "for end: "<< endl;
	return ret;
}
int main()
{
	int maxLen = 0;
	for (int begin = 0; begin < n; ++begin)
		maxLen = max(maxLen, lis2(begin));
	cout << "result: " << maxLen << endl;

	puts(" ");
	puts(" ");
	int maxLen3 = lis3(-1);
	cout << "result: " << maxLen3 << endl;

	getchar();
	return 0;
}