#include <iostream>
#include <fstream>
using namespace std;
int H;
int W;
int board[21][21];
enum {
	WHITE=0,
	BLACK=1,
};

const int boardtype[4][3][2] = {
	{{0,0},{0,1},{1,1}}, // ��
	{{0,0},{1,0},{1,1}}, // ��
	{{0,0},{1,0},{0,1}}, // |-
	{{0,0},{1,-1},{1,0}}, // _|
};

void disp()
{
	for (int i = 0; i < H; i++)
	{
		for (int j = 0; j < W; j++)
		{
			if (board[i][j] == WHITE)
				printf(".");
			else if (board[i][j] == BLACK)
				printf("#");
			else if (board[i][j] > BLACK)
				printf("?");
		}
		printf("\n");
	}
	printf("\n");
}

bool checkDuplicateStack()
{
	bool ret = false;
	for (int i = 0; i < H; i++)
	{
		for (int j = 0; j < W; j++)
		{
			if (board[i][j] > BLACK)
			{
				ret = true;
				break;
			}
		}
	}
	return ret;
}
bool checkAllcover()
{
	bool ret = true;
	for (int i = 0; i < H; i++)
	{
		for (int j = 0; j < W; j++)
		{
			if (board[i][j] > BLACK || board[i][j] == WHITE)
			{
				ret = false;
				break;
			}
		}
	}
	return ret;
}
int boardcover()
{
	int ret = 0;
	disp();
	int i, j;
	if (checkAllcover())
		return 1;
	if (checkDuplicateStack())
		return 0;
	//~ find White point
	for (i = 0; i < H; i++)
	{
		for (j = 0; j < W; j++)
		{
			if (board[i][j] == BLACK)
				continue;
			else
				break;
		}
		if (j != W)
			break;
	}
			//~ cover by 4 type 
	for (int k = 0; k < 4; k++)
	{
		//~ cover
		int h_temp1 = i + boardtype[k][0][0];
		int w_temp1 = j + boardtype[k][0][1];
		board[h_temp1][w_temp1]++;
		int h_temp2 = i + boardtype[k][1][0];
		int w_temp2 = j + boardtype[k][1][1];
		board[h_temp2][w_temp2]++;
		int h_temp3 = i + boardtype[k][2][0];
		int w_temp3 = j + boardtype[k][2][1];
		board[h_temp3][w_temp3]++;
		//if (checkDuplicateStack())
		//{
		//	printf("duplicate Stack!!!\n");
		//	continue;
		//}
		//~ call self
		ret += boardcover();
		//~ uncover
		board[h_temp1][w_temp1]--;
		board[h_temp2][w_temp2]--;
		board[h_temp3][w_temp3]--;
	}
	return ret;
}

int main()
{
	fstream myfile;
	myfile.open("boardcover.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		C = 1;
		for (int i = 0; i < C; i++)
		{
			//~ init
			memset(board, 0, sizeof(board));
			//~ input
			myfile >> H;
			myfile >> W;
			char ch;
			for (int j = 0; j < H; j++)
			{
				for (int k = 0; k < W; k++)
				{
					myfile >> ch;
					if (ch == '#')
						board[j][k] = BLACK;
					else if(ch == '.')
						board[j][k] = WHITE;
				}
			}
			//~ body
			int ret = boardcover();
			cout << ret << endl;
		}
		myfile.close();
	}
	getchar();
	return 0;
}