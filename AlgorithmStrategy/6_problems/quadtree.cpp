#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

#if 1
std::string quadtree(std::string& input)
{
	std::string output;
	if (input.size() == 1)
	{
		//std::cout << input[0];
		return input;
	}
	if (input[0] == 'x')
	{
		//printf("x");
		output = output + 'x';
		input = input.substr(1);
	}
	
	std::string lp;
	std::string rp;
	std::string ld;
	std::string rd;
	if (input[0] != 'x')
	{
		lp = input.substr(0, 1);
		//printf("%c\n", lp.c_str());
		input = input.substr(1);
	}
	else
		lp = quadtree(input);

	if (input[0] != 'x')
	{
		rp = input.substr(0, 1);
		input = input.substr(1);
	}
	else
		rp = quadtree(input);

	if (input[0] != 'x')
	{
		ld = input.substr(0, 1);
		input = input.substr(1);
	}
	else
		ld = quadtree(input);

	if (input[0] != 'x')
	{
		rd = input.substr(0, 1);
		input = input.substr(1);
	}
	else
		rd = quadtree(input);
	output = output + ld + rd + lp + rp;
	return output;
}

std::string quadtree2(std::string::iterator& it)
{
	char head = *it;
	++it;
	if (head == 'w' || head == 'b')
	{
		return std::string(1, head);
	}
	std::string upperLeft = quadtree2(it);
	std::string upperRight = quadtree2(it);
	std::string lowerLeft = quadtree2(it);
	std::string lowerRight = quadtree2(it);
	return std::string("x") + lowerLeft + lowerRight + upperLeft + upperRight;
}

void solveCase(std::fstream& fs)
{
	std::string input;
	std::string output;
	//std::getline(fs, input);
	fs >> input;
	std::string::iterator it = input.begin();
	//output = quadtree(input);
	output = quadtree2(it);
	std::cout << output << std::endl;
}

int main()
{
	std::fstream myfile;
	myfile.open("quadtree.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		//C = 1;
		for (int i = 0; i < C; i++)
		{
			//~ input
			//printf("%d\n", i);
			solveCase(myfile);
		}
		myfile.close();
	}
	getchar();
	return 0;
}
#endif

#if 0
int main()
{
	// greet the user
	std::string name;
	std::cout << "What is your name? ";
	std::getline(std::cin, name);
	std::cout << "Hello " << name << ", nice to meet you.\n";
	getchar();
	return 0;
}
#endif