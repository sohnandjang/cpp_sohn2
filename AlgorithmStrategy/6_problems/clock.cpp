#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

int clock[16];
constexpr int INF = 987654321;
constexpr int CLOCK_NUM = 16;
constexpr int SWITCH_NUM = 10;
const vector<int> clockInfo_switch[SWITCH_NUM] = 
{
	{0, 1, 2},
	{3, 7, 9, 11},
	{4, 10, 14, 15},
	{0, 4, 5, 6, 7},
	{6, 7, 8, 10, 12},
	{0, 2, 14, 15},
	{3, 14, 15},
	{4, 5, 7, 14, 15},
	{1, 2, 3, 4, 5},
	{3, 4, 5, 9, 13}
};

void pushSwitch(int i_orderClock, int numOfClick, bool i_plus)
{
	int value = 0;
	switch (i_plus)
	{
	case true: //plus
		value = clock[i_orderClock] + 3 * numOfClick;
		value %= 12;
		if (value == 0) value = 12;
		clock[i_orderClock] = value;
		break;
	case false: //minus
		value = clock[i_orderClock] - 3 * numOfClick + 12;
		value %= 12;
		if (value == 0) value = 12;
		clock[i_orderClock] = value;
		break;
	}
}

bool check12hour(int i_clock[])
{
	bool ret = true;
	for (int i = 0; i < CLOCK_NUM; i++)
	{
		if (i_clock[i] != 12)
		{
			ret = false;
			break;
		}
	}
	return ret;
}

void disp(int i_clock[])
{
	printf("clock:");
	for (int i = 0; i < CLOCK_NUM; i++)
		printf("%d, ", i_clock[i]);
	printf("\n");
}
int minSwitchPush(int i_clock[], int orderSwitch, int i_click)
{
	//disp(i_clock);
	int ret = INF;
	if (check12hour(i_clock))
		return i_click;

	if (orderSwitch >= SWITCH_NUM)
		return INF;

	for (int i = 0; i < 4; i++) 
	{
		//~ push
		int numOfClick = i % 4;
		for (const int& j : clockInfo_switch[orderSwitch])
			pushSwitch(j, numOfClick, true);
		
		//printf("-> orderSwitch:%d, click:%d, acc_click:%d\n", orderSwitch, numOfClick, i_click);
		ret = min(ret, minSwitchPush(i_clock, orderSwitch + 1, i_click + numOfClick));

		for (const int& j : clockInfo_switch[orderSwitch])
		{
			pushSwitch(j, numOfClick, false);
		}
	}
	return ret;
}

int solveCase(fstream& fs)
{
	for (int j = 0; j < CLOCK_NUM; j++)
	{
		fs >> clock[j];
	}
	int ret = minSwitchPush(clock, 0, 0);
	return ret;
}

int main()
{
	fstream myfile;
	myfile.open("clock.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		//C = 1;
		for (int i = 0; i < C; i++)
		{
			//~ input
			int ret = solveCase(myfile);
			cout << ret << endl;
		}
		myfile.close();
	}
	getchar();
	return 0;
}