# Clock
1. remember vector<int> V[10]; (vector array)  
2. constexpr  
3. const vector<int> V[10];  
4. for each in c++
``` c++
for(int& j: V[0])
	func(j);
==
for(int j=0; j<V[0].size(); j++)
	func(V[0][j]);
```
