#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>

int solve(int low, int high, std::vector<int>& h)
{
	int ret = 0;
	int direction = 0; //0:right, 1:left
	if (low == high) return h[low];
	int mid = (low + high) / 2;
	ret = std::max(ret, solve(low, mid, h));
	ret = std::max(ret, solve(mid+1, high, h));
	int midleft = mid;
	int midright = mid + 1;
	int midheight = std::min(h[mid], h[mid + 1]);
	int midSum = midheight * (midright - midleft + 1);
	while (1)
	{
		direction = 0; 
		if (midright >= high && midleft <= low) break;
		if (midright >= high)
			direction = 1; //left
		else if (midleft <= low)
			direction = 0; //right
		else
		{
			if (h[midleft - 1] < h[midright + 1])
				direction = 0; //right
			else
				direction = 1; //left
		}
		if (direction == 0) //right
		{
			midright++;
			midheight = std::min(midheight, h[midright]);
		}
		else
		{
			midleft--;
			midheight = std::min(midheight, h[midleft]);
		}
		midSum = std::max(midSum, midheight*(midright - midleft + 1));
		ret = std::max(ret, midSum);
	}
	return ret;
}

void caseSolve(std::fstream& fs)
{
	std::vector<int> h;
	int n;
	fs >> n;
	for (int i = 0; i < n; i++)
	{
		int temp;
		fs >> temp;
		h.push_back(temp);
	}
	int ret = solve(0, n - 1, h);
	std::cout << ret << std::endl;

}
int main()
{
	std::fstream myfile;
	myfile.open("fence.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		for (int i = 0; i < C; i++)
		{
			caseSolve(myfile);
		}

		myfile.close();
	}
	getchar();
	return 0;
}