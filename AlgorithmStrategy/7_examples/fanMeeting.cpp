#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <string>
using namespace std;

void normalize(vector<int>& num)
{
	num.push_back(0);
	for (int i = 0; i < num.size()-1; ++i)
	{
		if (num[i] < 0)
		{
			int borrow = (abs(num[i]) + 9) / 10;
			num[i + 1] -= borrow;
			num[i] += borrow * 10;
		}
		else
		{
			num[i + 1] += num[i] / 10;
			num[i] %= 10;
		}
	}
	while (num.size() > 1 && num.back() == 0) num.pop_back();
}

vector<int> multiply(const vector<int>& a, const vector<int>& b)
{
	vector<int> c(a.size() + b.size() + 1, 0);
	for (int i = 0; i < a.size(); i++)
		for (int j = 0; j < b.size(); j++)
			c[i + j] += a[i] * b[j];
	normalize(c);
	return c;
}

vector<int> multiply2(const vector<int>& a, const vector<int>& b)
{
	vector<int> c(a.size() + b.size() + 1, 0);
	for (int i = 0; i < a.size(); i++)
		for (int j = 0; j < b.size(); j++)
			c[i + j] += a[i] * b[j];
	return c;
}

void disp(vector<int>& a)
{
	for (size_t i = 0; i < a.size(); i++)
		printf("[%d] ", a[i]);
	printf("\n");
}
int countHug(std::string& member, std::string& fan)
{
	int memberLen = member.length();
	int fanLen = fan.length();
	int ret = 0;
	std::string strLong;
	std::string strShort; 
	if (memberLen >= fanLen)
	{
		strLong = member;
		strShort = fan;
	}
	else
	{
		strLong = fan;
		strShort = member;
	}
	int strLongLen = strLong.length();
	int strShortLen = strShort.length();

	// Man : 1, Female : 0 (Man-Man = 1, Man-Female = 0, Female-Female = 0 => if 1, hug fails)
	vector<int> vecLong;
	vector<int> vecShort;
	for (int i = 0; i < strLong.size(); i++)
	{
		if (strLong.substr(i, 1) == "M")
			vecLong.push_back(1);
		else
			vecLong.push_back(0);
	}
	for (int i = 0; i < strShort.size(); i++)
	{
		if (strShort.substr(i, 1) == "M")
			vecShort.push_back(1);
		else
			vecShort.push_back(0);
	}
	//reverse
	reverse(vecShort.begin(), vecShort.end());
	//printf("vecLong:"); disp(vecLong);  //printf("size = %d\n", vecLong.size());
	//printf("vecShort:"); disp(vecShort);//printf("size = %d\n", vecShort.size());

	vector<int> result = multiply2(vecLong, vecShort);
	//printf("result:"); disp(result);
	for (int i = vecShort.size() - 1; i <= vecLong.size() - 1; i++)
	{
		//printf("i = %d\n", i);
		if (result[i] == 0)
			ret++;
	}
	return ret;

	/*
	//std::cout << "LongLen: " << strLongLen << ", ShortLen: " << strShortLen << std::endl;
	//std::cout << "Loop: " << strLongLen - strShortLen + 1 << std::endl;
	for (int i = 0; i < strLongLen - strShortLen + 1; i++)
	{
		std::string strLongSub = strLong.substr(i, strShortLen);
		//std::cout << "Long: " << strLong << ", LongSub: " << strLongSub << ", Short: " << strShort << std::endl;
		int j = 0;
		for (j = 0; j < strShortLen; j++)
		{
			if (strLongSub[j] == 'M' && strShort[j] == 'M')
			{
				//std::cout << "Not hug" << std::endl;
				break;
			}
			//std::cout << "OK hug" << std::endl;
		}
		if (j != strShortLen)
			continue;
		ret++;
	}
	return ret;
	*/
}

void caseSolve(std::fstream& fs)
{
	std::string member;
	std::string fan;
	fs >> member;
	fs >> fan;
	int ret = countHug(member, fan);
	std::cout << ret << std::endl;

}

int main()
{
	std::fstream myfile;
	myfile.open("fanMeeting.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		//C = 1;
		for (int i = 0; i < C; i++)
		{
			caseSolve(myfile);
		}

		myfile.close();
	}
	getchar();
	return 0;
}
