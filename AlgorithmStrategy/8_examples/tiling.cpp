#include <iostream>
#include <string.h>
using namespace std;

const int MOD = 1000000007;
int cache[101];
int cache_sym[101];
int tiling(int n)
{
    int& ret = cache[n];
    if(ret != -1)
        return ret;

    ret = 0;
    if(n == 0)
        ret = 0;
    else if(n == 1)
        ret = 1;
    else if(n == 2)
        ret = 2;
    else
        ret = (tiling(n-1) + tiling(n-2)) % MOD;

    return ret;
}

int symTiling(int n)
{
    int& ret = cache_sym[n];
    if(ret != -1)
        return ret;

    ret = 0;
    if(n == 0)
        ret = 0;
    else if(n == 1)
        ret = 1;
    else if(n == 2)
        ret = 2;
    else
    {
        if(n % 2 == 0) {// even
            ret = (tiling(n/2) + tiling(n/2 -1 )) % MOD; // symtiling -> tiling
        }
        else{
            ret = tiling( (n-1)/2 ) % MOD; 
        }
    }
    return ret;
}

int asymmetric(int width)
{
    if(width % 2 == 1)
        return (tiling(width) - tiling(width/2) + MOD) % MOD;
    int ret = tiling(width);
    ret = (ret - tiling(width/2) + MOD) % MOD;
    ret = (ret - tiling(width/2 - 1) + MOD) % MOD;
    return ret;
}




int main()
{
    memset(cache, -1, sizeof(cache));
    memset(cache_sym, -1, sizeof(cache_sym));
    int num = 92;
    int result = asymmetric(num); //symTiling(5);//(tiling(num) - symTiling(num) + MOD)%MOD;
    cout << result << endl;
    cout << tiling(10) << ", " <<symTiling(10) << ", " << asymmetric(10) << endl;
    int result2 = (tiling(num) - symTiling(num) + MOD) % MOD;
    cout << result2 << endl;
    return 0;
}
