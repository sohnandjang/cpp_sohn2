#include <iostream>
#include <algorithm>
#include <string.h>
using namespace std;

int cache[101][101];
constexpr int MOD = 10000000;
// c : current number
// n : remained number
int poly(int c, int n)
{
    int& ret = cache[c][n]; //0;
    if(ret != -1)
        return ret;

    if(n == 0)
        return 1;

    ret = 0;
    for(int i=1; i<=n; i++)
    {
        if(c == 0){
            ret = (ret + poly(i, n-i)) % MOD;
        }
        else{
            ret = (ret + ((c + i -1) * poly(i, n-i)) % MOD) % MOD;
        }
    }
    //printf("total] (%d, %d) = %d\n", c, n, ret);
    return ret;
}


int main()
{
    memset(cache, -1, sizeof(cache));
    printf(">>> poly=%d\n", poly(0, 1));
    printf(">>> poly=%d\n", poly(0, 2));
    printf(">>> poly=%d\n", poly(0, 3));
    printf(">>> poly=%d\n", poly(0, 4));
    printf(">>> poly=%d\n", poly(0, 92));
    return 0;
}
