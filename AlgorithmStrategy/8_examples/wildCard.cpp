#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

bool wildCard(const string& A, const string& B)
{
	cout << "wildCard] A= " << A << ", B= " << B << endl;
	bool ret = false;
	string first = A.substr(0,1);
	//base
	if (A.size() == 1)
	{
		if (first == "*") return true;
		if (first == "?" && B.size() == 1) return true;
		if (B.size() == 1 && first == B.substr(0, 1)) return true;
	}

	// regression
	if (first == "?")
		ret = wildCard(A.substr(1, string::npos), B.substr(1, string::npos)); // A next element~
	else if (first == "*")
	{
		for (int i = 0; i < B.size(); i++)
		{
			ret |= wildCard(A.substr(1, string::npos), B.substr(i, string::npos));
		}
	}
	else
	{
		if (first == B.substr(0, 1))
			ret = wildCard(A.substr(1, string::npos), B.substr(1, string::npos));
		else
			ret = false;
	}
	return ret;
}

void caseSolve(fstream& fs)
{
	string myRegEx;
	vector<string> answer;
	fs >> myRegEx;
	int num;
	fs >> num;
	string candidate;
	for (int i = 0; i < num; i++)
	{
		fs >> candidate;
		//cout << "candidate: " << candidate << endl;
		bool ret = wildCard(myRegEx, candidate);
		//if(ret) push stack -> sort -> print
		if (ret == true)
		{
			//cout << "push: " << candidate << endl;
			answer.push_back(candidate);
		}
		else
		{
			//cout << "fail: " << candidate << endl;
		}

	}
	sort(answer.begin(), answer.end());
	for (int i = 0; i < answer.size(); i++)
		cout << answer[i] << endl;
	return;
}
int main()
{
	fstream myfile;
	myfile.open("wildCard.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		//C = 1;
		for (int i = 0; i < C; i++)
		{
			caseSolve(myfile);
		}

		myfile.close();
	}
	getchar();
	return 0;
}
