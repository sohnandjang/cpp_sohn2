# wildcard  
1. wildcard(string& A, string& B) -> brute force  
2. wildcard(int index_A, int index_B) -> dynamic programming  
3. Recursion  
4. Cache  

# polynomial 
1. poly(n) 
2. poly(n, first) 
because, first line affects the number of cases. 

