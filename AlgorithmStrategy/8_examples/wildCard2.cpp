#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

int cache[100][100] = { 0, };
string A;// myRegEx;
string B;// candidate;

int wildCard(int  indexA, int  indexB)
{
	//cout << "wildCard] A= " << indexA << ": " << A.substr(indexA,string::npos) <<  ", B= " << indexB << ": "<< B.substr(indexB, string::npos) << endl;
	int & ret = cache[indexA][indexB];
	if (cache[indexA][indexB] != -1)
		return ret;
	//string first = A.substr(0,1);
	//base
	if (A.size()-1 == indexA)
	{
		if (A.substr(indexA, 1) == "*") {
			//cout << "base * : ret = 1" << endl;
			return ret = 1;
		}
		if (A.substr(indexA,1)== "?" && B.size()-1 == indexB) return ret = 1;
		if (B.size()-1 == indexB && A.substr(indexA,1) == B.substr(indexB, 1)) return ret = 1;
	}

	// regression
	if (A.substr(indexA, 1)== "?")
		ret = wildCard(indexA + 1, indexB+1); // A next element~
	else if (A.substr(indexA,1) == "*")
	{
		for (int i = 0; i < B.size(); i++)
		{
			ret = wildCard(indexA+1, indexB+i);
			if (ret == 1)
				return ret;
		}
	}
	else
	{
		if (A.substr(indexA, 1) == B.substr(indexB, 1))
		{
			//cout << "here" << endl;
			ret = wildCard(indexA + 1, indexB + 1);
		}
		else
		{
			//cout << "there" << endl;
			ret = 0;
		}
	}
	return ret;
}

void caseSolve(fstream& fs)
{
	vector<string> answer;
	fs >> A;// myRegEx;
	int num;
	fs >> num;
	//num = 1;
	for (int i = 0; i < num; i++)
	{
		memset(cache, -1, sizeof(cache));
		fs >> B;// candidate;
		//cout << "candidate: " << candidate << endl;
		int ret = wildCard(0, 0);
		//printf("ret = %d\n");
		//if(ret) push stack -> sort -> print
		if (ret == 1)
		{
			//cout << "push: " << candidate << endl;
			answer.push_back(B);
		}
		else
		{
			//cout << "fail: " << candidate << endl;
		}

	}
	sort(answer.begin(), answer.end());
	for (int i = 0; i < answer.size(); i++)
		cout << answer[i] << endl;
	return;
}
int main()
{
	fstream myfile;
	myfile.open("wildCard.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		//C = 1;
		for (int i = 0; i < C; i++)
		{
			caseSolve(myfile);
		}

		myfile.close();
	}
	getchar();
	return 0;
}
