#include <iostream>
#include <vector>
#include <string.h>
using namespace std;

int C;
int N;
int D;
int P;
int N_RESULT;
vector<int> vecResHouse;
vector<vector<int>> adj;
double cache[50][100];

void init()
{
    cin >> N; cin >> D; cin >> P;
    printf("N=%d, D=%d, P=%d\n", N, D, P);
    adj.clear();
    vecResHouse.clear();
    memset(cache, -1, sizeof(cache));
    for(int i=0; i<N; ++i){
        vector<int> sub;
        for(int j=0; j<N; ++j){
            int temp;
            cin >> temp;
            if(temp == 1) // __TODO__
                sub.push_back(j);
        }
        adj.push_back(sub);
    }
    for(int i=0; i<N; ++i){
        //printf("adj[%d].size() = %d\n", i, adj[i].size());
    }
    cin >> N_RESULT;
    for(int i=0; i<N_RESULT; ++i){
        int temp;
        cin >> temp;
        vecResHouse.push_back(temp);
    }
}

double find(int curHouse, int day)
{
    double& ratio = cache[curHouse][day];
    if(ratio +1.0 < 0 ) // float/double  ratio > -0.5
        return ratio;
    ratio = 0.0;
    if(day == 0){ // base
        if(curHouse == P){
            return ratio=1.0;
        }
        else{
            return ratio=0.0;
        }
    }
    for(size_t i=0; i<adj[curHouse].size(); ++i){
        //printf("curHouse = %d\n", curHouse);
        int preHouse = adj[curHouse][i];
        //printf("preHouse = %d, size = %d\n", preHouse, adj[preHouse].size());
        ratio += 1.0/adj[preHouse].size() * find(preHouse, day-1);
        //printf("ratio = %lf\n", ratio);
    }
    return ratio;
}

void solve()
{
    init();
    for(size_t i=0; i<vecResHouse.size(); ++i){
        //printf("House to find: %d\n", vecResHouse[i]);
        double ret = find(vecResHouse[i], D);
        printf("%.8lf ", ret);
    }
    printf("\n");
}

int main()
{
   cin >> C;
   //C = 1;
   printf("C= %d\n", C);

   for(int i=0; i<C; ++i){
       solve();
   }

   return 0;
}
