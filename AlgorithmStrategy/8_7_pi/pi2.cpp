#include <iostream>
#include <string>
using namespace std;

string source[4] = { "11111222", "12122222", "12341357", "12673939" };
string input;
int classify(int start, int end)
{
	int len = end - start;
	string base = input.substr(start, end);

	cout << "base: " << base << endl;
	cout << "comp: " << string(len, base[0])<< endl;
	if (base == string(len, base[0])) return 1;
	bool progress = true;
	for (int i = 0; i < len - 1; i++)
	{
		if (base[1] - base[0] != base[i + 1] - base[i])
		{
			progress = false;
			break;
		}
	}
	cout << "base[1],[0]:" << base[1] << "," << base[0] << endl;
	if (progress == true && (abs(base[1] - base[0]) == 1)) return 2;
	bool cross = true;
	for (int i = 0; i < len; i++)
	{
		if (i % 2 == 0)
			if (base[i] != base[0])
			{
				cross = false;
				break;
			}

		if (i % 2 == 1)
			if (base[i] != base[1])
			{
				cross = false;
				break;
			}
	}
	if (cross == true) return 4;
	if (progress == true) return 5;
	return 10;
}

int main()
{
	int ret = 0;
	input = source[0];
	ret = classify(0, 4); // 1
	cout << ret << endl;
	input = source[2];
	ret = classify(0, 3); // 2
	cout << ret << endl;
	input = source[1];
	ret = classify(0, 3); // 4
	cout << ret << endl;
	input = source[2];
	ret = classify(4, 7); // 5
	cout << ret << endl;
	input = source[3];
	ret = classify(0, 3); // 10
	cout << ret << endl;
	getchar();
	return 0;
}