#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <stdio.h>
#include <limits.h>
using namespace std;
int maxValue = numeric_limits<int>::max();
int cache[10000];

vector<int> vec_A;
int N;

void disp()
{
	printf("disp:");
	for (int i = 0; i < vec_A.size(); i++)
		printf("%d ", vec_A[i]);
	printf("\n");
}


int getLevel(int start, int end)
{
	int ret = maxValue;
	if (N - start < 3)  // remained number : < 3 (failed)
		return maxValue;
	//int ret = 10;
	int nFirst = vec_A[start];
	int i = 0;
	//~1. all same
	for (i = start; i < end; i++)
	{
		if (nFirst != vec_A[i])
			break;
	}
	if (i == end) // not found
		return ret = 1;
	//~2. inc 1, dec 1
	int diff = (vec_A[start + 1] - vec_A[start])? 1: -1;
	for (i = start; i < end-1; i++)
	{
		if (diff != vec_A[i+ 1] - vec_A[i])
			break;
	}
	if (i == end-1)
		return ret = 2;
	//3. two number, one, the other
	int A = vec_A[start];
	int B = vec_A[start+1];
	for (i = start+2; i < end; i++)
	{
		if ((i - start) %2 == 0)
			if (A != vec_A[i])
				break;
		if ((i - start) %2 == 1)
			if (B != vec_A[i])
				break;
	}
	if (i == end)
		return ret = 4;
		


	//~4. inc , dec  same diff
	diff = vec_A[start + 1] - vec_A[start];
	for (i = start; i < end-1; i++)
	{
		if (diff != vec_A[i+ 1] - vec_A[i])
			break;
	}
	if (i == end-1)
		return ret = 5;

	return ret = 10;
}


int pi(int start)
{
	int &ret = cache[start];
	if (ret != -1)
		return  ret;
	ret = maxValue;
	if (N - start < 3)  // remained number : < 3 (failed)
		return maxValue;
	if (N == start + 3 || N == start + 4 || N == start + 5)
		return getLevel(start, N);
	ret = min(ret, getLevel(start, start + 3) + pi(start + 3));
	ret = min(ret, getLevel(start, start+4) + pi(start + 4));
	ret = min(ret, getLevel(start, start+5) + pi(start + 5));
	return ret;
}

void body(fstream& fs)
{
	// init
	vec_A.clear();
	N = 0;
	memset(cache, -1, sizeof(cache));

	// input
	string sInput;
	fs >> sInput;
	for (unsigned int i = 0; i < sInput.length(); i++)
	{
		int temp;
		string sTemp = sInput.substr(i, 1);
		temp = atoi(sTemp.c_str());
		vec_A.push_back(temp);
	}
	N = sInput.length();
	disp();

	// body function
	int ret = 0;
	ret = pi(0); 

	cout << ret << endl;
}

bool caseSolve(const char* path, int i_num) 
{// i_num == -1 , don't care
	fstream myfile;
	int num = 0;
	string abc;
	myfile.open(path);
	if (myfile.is_open())
	{
		myfile >> num;
		num = (i_num == -1 ? num : i_num);
		for (int i = 0; i < num; i++)
		{ //~ body
			body(myfile);
		}
		myfile.close();
		return true;
	}
	return false;
}

int main()
{
	caseSolve("pi.txt", -1);
	getchar();
	return 0;
}
