# mistakes

1. std::numeric_limits::max  use carefully  
	* it is ok if max keeps itself  
	* But there will be a problem if it plus something
		* overflow happens  

2. Visual Studio Error
	* excluding the file from build : tool bug
	* I spent my time for debugging (LNK error)
		* previous file and current file were built at the same time  

3. txt file
	* when I created txt file, file open error happened
		* I don't know why,
		* I retryed and corrected.
	* while I corrected, I changed text file into one for debugging
		* And I spent my valuable time because the value I read is different what I expected (one for debugging)

4. when I read string from file
	1. I tried fs >> temp (std::string temp) but I thought it didn't work -> works well!!!!
	2. getline -> it read from 1st line
	3. I prefer 1.(fs >> temp than getline)



